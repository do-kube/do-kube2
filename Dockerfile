# Build Stage
FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build-env

WORKDIR /generator

# restore
COPY ./dotnetcore.csproj .
RUN dotnet restore ./dotnetcore.csproj

#COPY src
COPY . .

# publish
RUN dotnet publish dotnetcore.csproj -o /publish

# Runtime Image Stage
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "dotnetcore.dll"]
